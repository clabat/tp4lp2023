<?php

namespace App\Controller\Admin;

use App\Entity\Concert;
use App\Entity\Email;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {

        $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);

        return $this->redirect($adminUrlGenerator->setController(ArtisteCrudController::class)->generateUrl());
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Adminstration de la Sirènre');

    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Artistes', 'fa fa-user');
        yield MenuItem::linkToCrud('Concerts', 'fa fa-volume-high', Concert::class);
        yield MenuItem::linkToCrud('Concerts passés', 'fa fa-volume-low', Concert::class)->setController (Concert2CrudController::class);
        yield MenuItem::linkToCrud('Newsletter', 'fa fa-envelope', Email::class);
        yield MenuItem::section ("");
        yield MenuItem::linkToRoute ('Retour au site','fa fa-home','app_homepage');
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
